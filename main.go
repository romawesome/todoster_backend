package main

import (
	"github.com/gin-gonic/gin"
	"github.com/kuzmrom7/todoster_backend/api"
	"github.com/kuzmrom7/todoster_backend/storage"
	"github.com/gin-contrib/cors"
)

func main() {

	storage.Init()
	r := gin.Default()

/*	r.Use(cors.Default())*/

		r.Use(cors.New(cors.Config{
			AllowOrigins:  []string{"http://localhost:3000"},
			AllowMethods:  []string{"GET", "POST", "PUT", "PATCH", "DELETE", "HEAD"},
			AllowHeaders:  []string{"Origin", "Content-Length", "Content-Type"},
		}))
	v1 := r.Group("/api")
	{
		v1.GET("/", api.FetchAllTodo)
		v1.POST("/", api.CreateTodo)
		v1.PUT("/")
		v1.DELETE("/")
	}

	r.Run(":8080")
}
