package storage

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
)


var DB *gorm.DB
var err error

func Init() {
	DB, err = gorm.Open("postgres", "host=localhost port=5432 user=todoster dbname=todoster password=todoster sslmode=disable")
	if err != nil {
		log.Println(err)
	}
	DB.AutoMigrate(&TodoModel{})

}